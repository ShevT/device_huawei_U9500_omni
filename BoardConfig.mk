#
# Copyright (C) 2012 The CyanogenMod Project
# Copyright (C) 2012 mdeejay
# Copyright (C) 2013 faust93
# Copyright (C) 2013-2015 ShevT
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

COMMON_FOLDER := device/huawei/omap4-common
VENDOR_DIR := vendor/huawei/front/proprietary

# inherit from the proprietary version
-include vendor/huawei/front/BoardConfigVendor.mk
# inherit from omap4
-include hardware/ti/huawei-omap4/BoardConfigCommon.mk
# inherit from common
-include $(COMMON_FOLDER)/BoardConfigCommon.mk
# inherit from front common
-include $(COMMON_FOLDER)/front-common/BoardConfigCommon.mk

BOARD_HARDWARE_CLASS := $(OMAP4_NEXT_FOLDER)/cmhw/

# Low RAM
TARGET_LOW_RAM_DEVICE := true
TARGET_EXCLUDE_LIVEWALLPAPERS := true

# Recovery
BOARD_SUPPRESS_SECURE_ERASE := true
# Graphics
TARGET_RECOVERY_PIXEL_FORMAT := "BGRA_8888"
TW_SCREEN_BLANK_ON_BOOT := true
TW_MAX_BRIGHTNESS := 250
TW_BRIGHTNESS_PATH := /sys/class/backlight/lcd/brightness
RECOVERY_GRAPHICS_USE_LINELENGTH := true
TW_THEME := portrait_hdpi
# Hardware
BOARD_HAS_NO_SELECT_BUTTON := true
TW_CUSTOM_CPU_TEMP_PATH := /sys/devices/platform/omap/pcb_temp_sensor.0/temp1_input
TW_CUSTOM_BATTERY_PATH := /sys/class/power_supply/Battery
# HW ID
TW_USE_MODEL_HARDWARE_ID_FOR_DEVICE_ID := false
TW_FORCE_CPUINFO_FOR_DEVICE_ID := false
# No Download mode
TW_HAS_DOWNLOAD_MODE := false
# Storages
BOARD_HAS_NO_MISC_PARTITION := true
TW_DEFAULT_EXTERNAL_STORAGE := true
TW_INTERNAL_STORAGE_PATH := "/sdcard"
TW_INTERNAL_STORAGE_MOUNT_POINT := "sdcard"
TW_EXTERNAL_STORAGE_PATH := "/extSdCard"
TW_EXTERNAL_STORAGE_MOUNT_POINT := "extSdCard"
TW_FLASH_FROM_STORAGE := true
TW_SDEXT_NO_EXT4 := true
RECOVERY_SDCARD_ON_DATA := true
# Crypto
TW_INCLUDE_CRYPTO := true
TW_INCLUDE_JB_CRYPTO := true
